import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import Vuetify from "vuetify/lib";
import Axios from "axios";
import VueInstagram from "vue-instagram";

import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

import firebase from "firebase"; // Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyC6suVwCDX5NW1zJ2U63Jf3aWnHLG-Zxss",
  authDomain: "vue-tutorial-85530.firebaseapp.com",
  databaseURL: "https://vue-tutorial-85530.firebaseio.com",
  projectId: "vue-tutorial-85530",
  storageBucket: "vue-tutorial-85530.appspot.com",
  messagingSenderId: "389458949782",
  appId: "1:389458949782:web:4aa55ed999f9d5561634b7",
  measurementId: "G-NCJPSNN108"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Vuetify);
Vue.use(VueInstagram);

Vue.config.productionTip = false;

// set auth header
Axios.defaults.headers.common["Authorization"] = `Bearer ${store.state.token}`;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
