import Vue from "vue";
import Router from "vue-router";

import firebase from "firebase";

import Home from "../views/Home";
import Login from "../views/Login";
import SignUp from "../views/SignUp";
import Admin from "../views/Admin";
import Driver from "../views/Driver";
import Customer from "../views/Customer";
import profile from "../views/profile";
import AppNavigation from "../components/AppNavigation";
import trydemo from "../components/trydemo";
import ForgotPassword from "../components/ForgotPassword";

import Banquets from "../views/Banquets";
import Photographer from "../views/Photographer";
import Boutique from "../views/Boutique";
import Music from "../views/Music";
import Gallery from "../views/Gallery";

Vue.use(Router);

let router = new Router({
  routes: [
    {
      path: "/",
      name: "trydemo",
      component: trydemo,
      meta: {
        guest: true
      }
    },
    {
      path: "/SignUp",
      name: "SignUp",
      component: SignUp,
      meta: {
        guest: true
      }
    },
    {
      path: "/Login",
      name: "Login",
      component: Login,
      meta: {
        guest: true
      }
    },

    {
      path: "/admin",
      name: "admin",
      component: Admin,
      meta: {
        auth: true
      }
    },
    {
      path: "/driver",
      name: "driver",
      component: Driver,
      meta: {
        auth: true
      }
    },
    {
      path: "/customer",
      name: "customer",
      component: Customer,
      meta: {
        auth: true
      }
    },
    {
      path: "/profile",
      name: "profile",
      component: profile
    },
    {
      path: "/AppNavigation",
      name: "AppNavigation",
      component: AppNavigation
    },
    {
      path: "/Home",
      name: "Home",
      component: Home
    },
    {
      path: "/ForgotPassword",
      name: "ForgotPassword",
      component: ForgotPassword
    },
    {
      path: "/Banquets",
      name: "Banquets",
      component: Banquets
    },
    {
      path: "/Photographer",
      name: "Photographer",
      component: Photographer
    },
    {
      path: "/Boutique",
      name: "Boutique",
      component: Boutique
    },
    {
      path: "/Music",
      name: "Music",
      component: Music
    },
    {
      path: "/Gallery",
      name: "Gallery",
      component: Gallery
    }
  ]
});

router.beforeEach((to, from, next) => {
  firebase.auth().onAuthStateChanged(userAuth => {
    if (userAuth) {
      firebase
        .auth()
        .currentUser.getIdTokenResult()
        .then(function({ claims }) {
          if (claims.customer) {
            if (to.path !== "/customer")
              return next({
                path: "/customer"
              });
          } else if (claims.admin) {
            if (to.path !== "/admin")
              return next({
                path: "/admin"
              });
          } else if (claims.driver) {
            if (to.path !== "/driver")
              return next({
                path: "/driver"
              });
          }
        });
    } else {
      if (to.matched.some(record => record.meta.auth)) {
        next({
          path: "/login",
          query: {
            redirect: to.fullPath
          }
        });
      } else {
        next();
      }
    }
  });

  next();
});

export default router;
